// Learn more about F# at http://docs.microsoft.com/dotnet/fsharp

open System
open System.IO
open System.Text.RegularExpressions

type Codon = string

let (|Regex|_|) pattern input =
    let m = Regex.Match(input, pattern)
    if m.Success then Some(List.tail [ for g in m.Groups -> g.Value ])
    else None

module AminoLib =
    type AminoAcid = 
        | Phenylalanine
        | Leucine
        | Isoleucine
        | Methionine
        | Valine        
        | Proline
        | Threonine
        | Alanine
        | Tyrosine
        | Stop
        | Histidine
        | Glutamine 
        | Asparagine 
        | Lysine
        | AsparticAcid 
        | GlutamicAcid 
        | Cysteine
        | Tryptophan         
        | Serine
        | Arginine
        | Glycine 

    let private symbol2Name (c : char) : AminoAcid =
        match c with       
        | 'A' -> Alanine
        | 'C' -> Cysteine
        | 'D' -> AsparticAcid
        | 'E' -> GlutamicAcid
        | 'F' -> Phenylalanine
        | 'G' -> Glycine
        | 'H' -> Histidine
        | 'I' -> Isoleucine
        | 'K' -> Lysine
        | 'L' -> Leucine
        | 'M' -> Methionine
        | 'N' -> Asparagine
        | 'P' -> Proline
        | 'Q' -> Glutamine
        | 'R' -> Arginine
        | 's' -> Stop
        | 'S' -> Serine
        | 'T' -> Threonine
        | 'V' -> Valine
        | 'W' -> Tryptophan
        | 'Y' -> Tyrosine
        | _ -> sprintf @"unknown AA '%c'" c |> failwith

   

    let parse mappingTablePath =
        let parseLine l =
            match l with
            | Regex @"(\w),(\w{3})" [symbol; codon] -> (codon, char symbol |> symbol2Name)
            | _ -> sprintf "bad entry in mapingtable %s" l |> failwith

        Seq.map parseLine mappingTablePath

    let private mappingTable = 
        let content = File.ReadAllLines(@"C:\PRIV\gitlab\v2v\data\codon2aa.csv")
        content |> Seq.skip 1 |> parse |> Map.ofSeq 

    let codonToAmino (codon : Codon) : AminoAcid option =
        Map.tryFind codon mappingTable



let isSameAminoAcid codon1 codon2 =
    if codon1 = codon2 then
        true
    else
        let amino1 = AminoLib.codonToAmino codon1
        let amino2 = AminoLib.codonToAmino codon2
        match (amino1, amino2) with
        | (Some a1, Some a2) -> a1 = a2
        | _ -> false

let optimizeEfficiency (codon : Codon) : Codon = 
    let listCodon = codon |> Seq.toList
    let candidates = 
        match listCodon with
        | _ :: _ :: ['C'] -> [codon]
        | _ :: _ :: ['G'] -> [codon]
        | c1 :: c2 :: [_] -> [new string [|c1; c2; 'C'|] ; new string [|c1; c2; 'G'|] ; codon ]
        | _ -> sprintf "bad codon %s" codon  |> failwith 
    candidates |> List.find (isSameAminoAcid codon)

let evaluate (prediction : Codon list) (truth : Codon list) =
    let both = List.zip prediction truth
    let countSame = both |> List.filter (fun t -> fst t = snd t) |> List.length
    (float countSame) / (float both.Length)

let parse (filePath: string) : (Codon list * Codon list) =
    let content = File.ReadAllLines(filePath)
    
    let parseLine l = 
        match l with
        | Regex @"^(\d+),(\w+),(\w+)$" [_; origin; vaccine] -> (origin, vaccine)
        | _ -> sprintf "bad entry %s" l |> failwith
    
    let proteins = content |> Seq.skip 1 |> Seq.map parseLine
    let codonOrig = proteins |> Seq.map fst |> Seq.toList
    let codonVaccine = proteins |> Seq.map snd |> Seq.toList
    (codonOrig, codonVaccine)

[<EntryPoint>]
let main argv =
    let (origin, vaccin) = parse @"C:\PRIV\gitlab\v2v\data\groundtrouth.csv"    
    let optimized = origin |> List.map optimizeEfficiency
    printfn "Input : %A" optimized
    let perf = evaluate optimized (vaccin)
    printfn "Performance : %f" perf
    0 // return an integer exit code


 // let private mappingTable = 
    //     Map.ofList 
    //         [ 
    //             ("UUU", Phenylalanine) 
    //             ("UUC", Phenylalanine) 
    //             ("UUA", Leucine) 
    //             ("UUG", Leucine) 
    //             ("CUU", Leucine) 
    //             ("CUC", Leucine) 
    //             ("CUA", Leucine) 
    //             ("CUG", Leucine) 
    //             ("AUU", Isoleucine) 
    //             ("AUC", Isoleucine) 
    //             ("AUA", Isoleucine) 
    //             ("AUG", Methionine) 
    //             ("GUU", Valine) 
    //             ("GUC", Valine) 
    //             ("GUA", Valine) 
    //             ("GUG", Valine) 
    //             ("UCU", Serine) 
    //             ("UCC", Serine) 
    //             ("UCA", Serine) 
    //             ("UCG", Serine) 
    //             ("CCU", Proline) 
    //             ("CCC", Proline) 
    //             ("CCA", Proline) 
    //             ("CCG", Proline) 
    //             ("ACU", Threonine) 
    //             ("ACC", Threonine) 
    //             ("ACA", Threonine) 
    //             ("ACG", Threonine) 
    //             ("GCU", Alanine) 
    //             ("GCC", Alanine) 
    //             ("GCA", Alanine) 
    //             ("GCG", Alanine) 
    //             ("UAU", Tyrosine) 
    //             ("UAC", Tyrosine) 
    //             ("UAA", Stop) 
    //             ("UAG", Stop) 
    //             ("CAU", Histidine) 
    //             ("CAC", Histidine) 
    //             ("CAA", Glutamine) 
    //             ("CAG", Glutamine) 
    //             ("AAU", Asparagine) 
    //             ("AAC", Asparagine) 
    //             ("AAA", Lysine) 
    //             ("AAG", Lysine) 
    //             ("GAU", AsparticAcid) 
    //             ("GAC", AsparticAcid) 
    //             ("GAA", GlutamicAcid) 
    //             ("GAG", GlutamicAcid) 
    //             ("UGU", Cysteine) 
    //             ("UGC", Cysteine) 
    //             ("UGA", Stop) 
    //             ("UGG", Tryptophan) 
    //             ("CGU", Arginine) 
    //             ("CGC", Arginine) 
    //             ("CGA", Arginine) 
    //             ("CGG", Arginine) 
    //             ("AGU", Serine) 
    //             ("AGC", Serine) 
    //             ("AGA", Arginine) 
    //             ("AGG", Arginine) 
    //             ("GGU", Glycine) 
    //             ("GGC", Glycine) 
    //             ("GGA", Glycine) 
    //             ("GGG", Glycine)                 
    //         ]